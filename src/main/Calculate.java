package main;

public class Calculate {

    public <T extends Number, P extends Number> String plus(T t, P p){
        return String.valueOf(t.doubleValue() + p.doubleValue());
    }

    public <T extends Number, P extends Number> String minus(T t, P p){
        return String.valueOf(t.doubleValue() - p.doubleValue());
    }

    public <T extends Number, P extends Number> String divide(T t, P p){
        return String.valueOf(t.doubleValue() / p.doubleValue());
    }

    public <T extends Number, P extends Number> String multiply(T t, P p){
        return String.valueOf(t.doubleValue() * p.doubleValue());
    }
}
