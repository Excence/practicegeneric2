package main;

public class Main {

    public static void main(String[] args) {
        Calculate calculate = new Calculate();

        int i = 3;
        float f = 6.7f;

        System.out.println(calculate.plus(i, f));
        System.out.println(calculate.minus(i, f));
        System.out.println(calculate.divide(i, f));
        System.out.println(calculate.multiply(i, f));
    }
}
